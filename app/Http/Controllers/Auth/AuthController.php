<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\UserRepositoryInterface;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    //

    /** Group of model as vars */
    protected $userRepositoryInterface;


    public function __construct(UserRepositoryInterface $userRepository){

        $this->userRepositoryInterface = $userRepository;
    }



    public function login(){

        return $this->userRepositoryInterface->login();
    }


    public function postLogin(Request $request){

        return $this->userRepositoryInterface->postLogin($request);
    }


    public function register(){

        return $this->userRepositoryInterface->register();
    }


    public function postRegister(Request $request){

        return $this->userRepositoryInterface->postRegister($request);
    }


    public function verify($token){
        return $this->userRepositoryInterface->verify($token);
    }



    public function logout(){
        return $this->userRepositoryInterface->logout();
    }



}

