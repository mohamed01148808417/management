<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServicesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //

        $this->app->bind(
            'App\Http\Interfaces\UserRepositoryInterface',
            'App\Http\Repositories\UserRepository'
        );


        $this->app->bind(
            'App\Http\Interfaces\HomeRepositoryInterface',
            'App\Http\Repositories\HomeRepository'
        );

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
