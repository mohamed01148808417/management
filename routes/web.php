<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@dashboard');
Route::group(['namespace'=>'Auth'], function (){
    Route::get('login', 'AuthController@login')->name('login');
    Route::post('post-login', 'AuthController@postLogin')->name('login.post');
    Route::get('registration', 'AuthController@register')->name('register');
    Route::post('post-registration', 'AuthController@postRegister')->name('register.post');
    Route::get('logout', 'AuthController@logout')->name('logout');
    Route::get('account/verify/{token}', 'AuthController@verify')->name('user.verify');
});



/* New Added Routes */
Route::get('dashboard', 'HomeController@dashboard')->middleware(['auth', 'is_verify_email'])->name('dashboard');

