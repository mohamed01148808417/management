<?php
namespace App\Http\Interfaces;

interface UserRepositoryInterface{


    /*
     * User login
     * */
    public function login();

    public function postLogin($request);




    /*
     * User Register
     * */

    public function register();


    public function postRegister($request);


    public function create($array);


    /*
     * User Verify by token
     * */

    public function verify($token);



    /*
     * Logout
     * */


    public function logout();


}