<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 13/08/21
 * Time: 07:41 م
 */

namespace App\Http\Repositories;

use App\Http\Interfaces\UserRepositoryInterface;
use App\Models\User;
use App\Models\UserVerify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Session;


class UserRepository implements UserRepositoryInterface
{

    public $user;



    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function login()
    {
        // TODO: Implement login() method.

        return view('auth.login');
    }


    public function postLogin($request)
    {
        // TODO: Implement postLogin() method.

        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->intended('dashboard')
                ->withSuccess('You have Successfully loggedin');
        }

        return redirect("login")->withSuccess('Oppes! You have entered invalid credentials');

    }


    public function register()
    {
        // TODO: Implement register() method.

        return view('auth.register');
    }

    public function postRegister($request)
    {
        // TODO: Implement postRegister() method.
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);

        $data = $request->all();

        $check = $this->create($data);

        $token = Str::random(64);



        UserVerify::create([

            'user_id' => $check->id,
            'token' => $token

        ]);


        try{
            Mail::send('emails.emailVerificationEmail', ['token' => $token], function($message) use($request){

                $message->to($request->email);
                $message->subject('Email Verification Mail');

            });
        }catch ( \Exception $e){
            $e->getMessage();
        }


        return redirect("login")->withSuccess('Great! You have Successfully register check mail to active account ');
    }

    public function create($data)
    {
        // TODO: Implement create() method.

        return $this->user::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
    }


    public function verify($token)
    {
        // TODO: Implement verify() method.

        $verifyUser = UserVerify::where('token', $token)->first();



        $message = 'Sorry your email cannot be identified.';



        if(!is_null($verifyUser) ){

            $user = $verifyUser->user;


            if(!$user->is_email_verified) {

                $verifyUser->user->is_email_verified = 1;

                $verifyUser->user->save();

                $verifyUser->delete();

                $message = "Your e-mail is verified. You can now login.";

                try{
                    $admins = $this->user::where('is_admin',1)->get();
                    foreach ($admins as $admin){
                        Mail::send('emails.adminNotify',['userEmail' => $verifyUser->user->email], function($message) use ($admin) {
                            $message->to($admin->email);
                            $message->subject('Notification User Active');
                        });

                    }
                }catch (\Exception $e){

                    // Never reached

                    $e->getMessage();
                }

            } else {

                $message = "Your e-mail is already verified. You can now login.";

            }

        }

        return redirect()->route('login')->with('message', $message);

    }



    public function logout()
    {
        // TODO: Implement logout() method.

        Session::flush();
        Auth::logout();

        return Redirect('login');
    }

}