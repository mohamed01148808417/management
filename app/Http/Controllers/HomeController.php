<?php

namespace App\Http\Controllers;
use App\Http\Interfaces\HomeRepositoryInterface;

class HomeController extends Controller
{


    /** Group of model as vars */
    protected $homeRepositoryInterface;


    public function __construct(HomeRepositoryInterface $homeRepository){

        $this->homeRepositoryInterface = $homeRepository;
    }



    public function dashboard(){
        return $this->homeRepositoryInterface->dashboard();
    }



}
